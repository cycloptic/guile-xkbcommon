# guile-xkbcommon

[![License: GPL 3.0 or later][license-img]][license-spdx]

A set of [GNU Guile][guile] bindings for the [xkbcommon][xkbcommon] keyboard
handling library. See the unit tests in the `tests` folder for usage examples.

# Building

Requirements:

- autotools
- guile >=3.0
- libxkbcommon

Build steps:

```sh
autoreconf -fiv
./configure
make
make install
```

[guile]: https://www.gnu.org/software/guile/
[xkbcommon]: https://xkbcommon.org/

[license-img]:  https://img.shields.io/badge/License-GPL%203.0%20or%20later-blue.svg?logo=gnu
[license-spdx]: https://spdx.org/licenses/GPL-3.0-or-later.html
