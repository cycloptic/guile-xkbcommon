;;;; -*- coding: utf-8; mode: scheme -*-
;;;; SPDX-FileCopyrightText: 2020 Jason Francis <jason@cycles.network>
;;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (xkbcommon keysyms))

(eval-when (expand load eval)
  (load-extension "libguile-xkbcommon" "scm_init_xkbcommon_keysyms"))
