/* SPDX-FileCopyrightText: 2020 Jason Francis <jason@cycles.network>
 * SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <errno.h>
#include <libguile.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <xkbcommon/xkbcommon.h>

#include "guile-xkbcommon.h"

SCM scm_xkb_rule_names_type;
SCM scm_xkb_context_type;
SCM scm_xkb_keymap_type;
SCM scm_xkb_state_type;

static SCM xkb_context_log_func_table;

static SCM k_case_insensitive;
static SCM k_no_default_includes;
static SCM k_no_environment_names;
static SCM k_gtk;

static SCM sym_rules;
static SCM sym_model;
static SCM sym_layout;
static SCM sym_variant;
static SCM sym_options;

static SCM k_rules;
static SCM k_model;
static SCM k_layout;
static SCM k_variant;
static SCM k_options;

static inline int scm_proc_supports_arity(SCM proc, int argc) {
  if (!scm_is_true(scm_procedure_p(proc)))
    return 0;
  SCM arities = scm_procedure_minimum_arity(proc);
  if (!scm_is_false(arities)) {
    int req = scm_to_int(SCM_CAR(arities));
    int opt = scm_to_int(SCM_CADR(arities));
    int rest = scm_to_bool(SCM_CADDR(arities));
    return !(req > argc || (!rest && req + opt < argc));
  }
  abort();
}

#define FUNC_NAME subr
static inline void
scm_validate_proc_arity(const char *subr, int pos, SCM proc, int argc) {
  if (SCM_UNLIKELY(!scm_proc_supports_arity(proc, argc)))
    scm_error(scm_arg_type_key, FUNC_NAME,
        "Wrong type argument at position ~A (expecting procedure of arity ~A), got: ~S",
        scm_list_3(scm_from_int(pos), scm_from_int(argc), proc),
        scm_list_1(proc));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keysym_get_name
SCM_DEFINE_PUBLIC(scm_xkb_keysym_get_name, "xkb-keysym-get-name", 1, 0, 0,
    (SCM keysym),
    "") {
  xkb_keysym_t i_keysym;
  SCM_VALIDATE_UINT_COPY(SCM_ARG1, keysym, i_keysym);
  char buffer[128];
  int size = xkb_keysym_get_name(i_keysym, buffer, sizeof(buffer));
  if (size < 0)
    return SCM_BOOL_F;
  return scm_from_utf8_stringn(buffer, size);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keysym_from_name
SCM_DEFINE_PUBLIC(scm_xkb_keysym_from_name, "xkb-keysym-from-name", 1, 0, 1,
    (SCM name, SCM rest),
    "") {
  SCM_VALIDATE_STRING(SCM_ARG1, name);

  SCM case_insensitive = SCM_BOOL_F;
  scm_c_bind_keyword_arguments(FUNC_NAME, rest, 0,
      k_case_insensitive, &case_insensitive,
      SCM_UNDEFINED);
  enum xkb_keysym_flags flags = scm_is_false(case_insensitive)
    ? XKB_KEYSYM_NO_FLAGS : XKB_KEYSYM_CASE_INSENSITIVE;

  char *i_name = scm_to_utf8_string(name);
  xkb_keysym_t ret = xkb_keysym_from_name(i_name, flags);
  free(i_name);
  if (ret == XKB_KEY_NoSymbol)
    return SCM_BOOL_F;
  return scm_from_uint32(ret);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keysym_to_upper
SCM_DEFINE_PUBLIC(scm_xkb_keysym_to_upper, "xkb-keysym-to-upper", 1, 0, 0,
    (SCM keysym),
    "") {
  xkb_keysym_t i_keysym;
  SCM_VALIDATE_UINT_COPY(SCM_ARG1, keysym, i_keysym);
  return scm_from_uint32(xkb_keysym_to_upper(i_keysym));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keysym_to_lower
SCM_DEFINE_PUBLIC(scm_xkb_keysym_to_lower, "xkb-keysym-to-lower", 1, 0, 0,
    (SCM keysym),
    "") {
  xkb_keysym_t i_keysym;
  SCM_VALIDATE_UINT_COPY(SCM_ARG1, keysym, i_keysym);
  return scm_from_uint32(xkb_keysym_to_lower(i_keysym));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keysym_to_utf8
SCM_DEFINE_PUBLIC(scm_xkb_keysym_to_utf8, "xkb-keysym-to-utf8", 1, 0, 0,
    (SCM keysym),
    "") {
  xkb_keysym_t i_keysym;
  SCM_VALIDATE_UINT_COPY(SCM_ARG1, keysym, i_keysym);
  char buffer[128];
  int size = xkb_keysym_to_utf8(i_keysym, buffer, sizeof(buffer));
  if (size < 0)
    return SCM_BOOL_F;
  buffer[size] = '\0';
  return scm_from_utf8_stringn(buffer, size - 1);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keysym_to_utf32
SCM_DEFINE_PUBLIC(scm_xkb_keysym_to_utf32, "xkb-keysym-to-utf32", 1, 0, 0,
    (SCM keysym),
    "") {
  xkb_keysym_t i_keysym;
  SCM_VALIDATE_UINT_COPY(SCM_ARG1, keysym, i_keysym);
  uint32_t c = xkb_keysym_to_utf32(i_keysym);
  if (!c)
    return SCM_BOOL_F;
  return scm_c_make_char(c);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_utf32_to_keysym
SCM_DEFINE_PUBLIC(scm_xkb_utf32_to_keysym, "xkb-utf32-to-keysym", 1, 0, 0,
    (SCM ucs),
    "") {
  scm_t_wchar i_ucs;
  SCM_VALIDATE_CHAR_COPY(SCM_ARG1, ucs, i_ucs);
  xkb_keysym_t sym = xkb_utf32_to_keysym(i_ucs);
  if (sym == XKB_KEY_NoSymbol)
    return SCM_BOOL_F;
  return scm_from_uint32(sym);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_make_xkb_context
SCM_DEFINE_PUBLIC(scm_make_xkb_context, "make-xkb-context", 0, 0, 1,
    (SCM rest),
    "") {
  enum xkb_context_flags flags = XKB_CONTEXT_NO_FLAGS;
  SCM no_default_includes = SCM_BOOL_F;
  SCM no_environment_names = SCM_BOOL_F;

  scm_c_bind_keyword_arguments(FUNC_NAME, rest, 0,
      k_no_default_includes, &no_default_includes,
      k_no_environment_names, &no_environment_names,
      SCM_UNDEFINED);
  if (!scm_is_false(no_default_includes))
    flags |= XKB_CONTEXT_NO_DEFAULT_INCLUDES;
  if (!scm_is_false(no_environment_names))
    flags |= XKB_CONTEXT_NO_ENVIRONMENT_NAMES;

  struct xkb_context *c_context = xkb_context_new(flags);
  if (!c_context)
    scm_report_out_of_memory();
  return scm_make_foreign_object_1(scm_xkb_context_type, c_context);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_context_unref
SCM_DEFINE_PUBLIC(scm_xkb_context_unref, "xkb-context-unref", 1, 0, 0,
    (SCM context),
    "") {
  SCM_ASSERT_TYPE(SCM_IS_A_P(context, scm_xkb_context_type),
      context, SCM_ARG1, FUNC_NAME, "<xkb-context>");
  struct xkb_context *c_context = scm_foreign_object_ref(context, 0);
  if (c_context) {
    scm_hash_remove_x(xkb_context_log_func_table, scm_from_pointer(context, NULL));
    xkb_context_unref(c_context);
    scm_foreign_object_set_x(context, 0, NULL);
  }
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_context_include_path_append
SCM_DEFINE_PUBLIC(scm_xkb_context_include_path_append,
    "xkb-context-include-path-append", 2, 0, 0,
    (SCM context, SCM path),
    "") {
  struct xkb_context *i_context;
  SCM_VALIDATE_XKB_CONTEXT_COPY(SCM_ARG1, context, i_context);
  SCM_VALIDATE_STRING(SCM_ARG2, path);
  char *i_path = scm_to_utf8_string(path);
  int status = xkb_context_include_path_append(i_context, i_path);
  free(i_path);
  return scm_from_bool(status);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_context_include_path_append_default
SCM_DEFINE_PUBLIC(scm_xkb_context_include_path_append_default,
    "xkb-context-include-path-append-default", 1, 0, 0,
    (SCM context),
    "") {
  struct xkb_context *i_context;
  SCM_VALIDATE_XKB_CONTEXT_COPY(SCM_ARG1, context, i_context);
  return scm_from_bool(xkb_context_include_path_append_default(i_context));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_context_include_path_reset_defaults
SCM_DEFINE_PUBLIC(scm_xkb_context_include_path_reset_defaults,
    "xkb-context-include-path-reset-defaults", 1, 0, 0,
    (SCM context),
    "") {
  struct xkb_context *i_context;
  SCM_VALIDATE_XKB_CONTEXT_COPY(SCM_ARG1, context, i_context);
  return scm_from_bool(xkb_context_include_path_reset_defaults(i_context));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_context_include_path_clear
SCM_DEFINE_PUBLIC(scm_xkb_context_include_path_clear,
    "xkb-context-include-path-clear", 1, 0, 0,
    (SCM context),
    "") {
  struct xkb_context *i_context;
  SCM_VALIDATE_XKB_CONTEXT_COPY(SCM_ARG1, context, i_context);
  xkb_context_include_path_clear(i_context);
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_context_include_paths
SCM_DEFINE_PUBLIC(scm_xkb_context_include_paths,
    "xkb-context-include-paths", 1, 0, 0,
    (SCM context),
    "") {
  struct xkb_context *i_context;
  SCM_VALIDATE_XKB_CONTEXT_COPY(SCM_ARG1, context, i_context);
  unsigned int count = xkb_context_num_include_paths(i_context);
  SCM paths = SCM_EOL;
  for (unsigned int i = 0; i < count; i++) {
    const char *path = xkb_context_include_path_get(i_context, count - 1 - i);
    paths = scm_cons(scm_from_utf8_string(path), paths);
  }
  return paths;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_context_set_log_level_x
SCM_DEFINE_PUBLIC(scm_xkb_context_set_log_level_x,
    "xkb-context-set-log-level!", 2, 0, 0,
    (SCM context, SCM level),
    "") {
  struct xkb_context *i_context;
  SCM_VALIDATE_XKB_CONTEXT_COPY(SCM_ARG1, context, i_context);
  enum xkb_log_level i_level;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, level, i_level);
  xkb_context_set_log_level(i_context, i_level);
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_context_get_log_level
SCM_DEFINE_PUBLIC(scm_xkb_context_get_log_level,
    "xkb-context-get-log-level", 1, 0, 0,
    (SCM context),
    "") {
  struct xkb_context *i_context;
  SCM_VALIDATE_XKB_CONTEXT_COPY(SCM_ARG1, context, i_context);
  return scm_from_uint(xkb_context_get_log_level(i_context));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_context_set_log_verbosity_x
SCM_DEFINE_PUBLIC(scm_xkb_context_set_log_verbosity_x,
    "xkb-context-set-log-verbosity!", 2, 0, 0,
    (SCM context, SCM verbosity),
    "") {
  struct xkb_context *i_context;
  SCM_VALIDATE_XKB_CONTEXT_COPY(SCM_ARG1, context, i_context);
  int i_verbosity;
  SCM_VALIDATE_INT_COPY(SCM_ARG2, verbosity, i_verbosity);
  xkb_context_set_log_verbosity(i_context, i_verbosity);
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_context_get_log_verbosity
SCM_DEFINE_PUBLIC(scm_xkb_context_get_log_verbosity,
    "xkb-context-get-log-verbosity", 1, 0, 0,
    (SCM context),
    "") {
  struct xkb_context *i_context;
  SCM_VALIDATE_XKB_CONTEXT_COPY(SCM_ARG1, context, i_context);
  return scm_from_int(xkb_context_get_log_verbosity(i_context));
}
#undef FUNC_NAME

#define FUNC_NAME "xkb-context-log-fn"
static void context_log(struct xkb_context *c_context, enum xkb_log_level level,
                        const char *format, va_list args) {
  char *str;
  int ret = vasprintf(&str, format, args);
  if (ret < 0)
    scm_syserror(FUNC_NAME);
  SCM proc = scm_hash_ref(xkb_context_log_func_table,
                          scm_from_pointer(c_context, NULL), SCM_BOOL_F);
  if (SCM_UNLIKELY(!scm_is_true(scm_procedure_p(proc))))
    return;
  scm_call_2(proc, scm_from_uint(level), scm_from_utf8_string(str));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_context_set_log_fn_x
SCM_DEFINE_PUBLIC(scm_xkb_context_set_log_fn_x,
    "xkb-context-set-log-fn!", 2, 0, 0,
    (SCM context, SCM log_fn),
    "") {
  struct xkb_context *c_context;
  SCM_VALIDATE_XKB_CONTEXT_COPY(SCM_ARG1, context, c_context);
  SCM ptr = scm_from_pointer(c_context, NULL);
  if (scm_is_false(log_fn)) {
    scm_hash_remove_x(xkb_context_log_func_table, ptr);
    xkb_context_set_log_fn(c_context, NULL);
  } else {
    scm_validate_proc_arity(FUNC_NAME, SCM_ARG2, log_fn, 2);
    scm_hash_set_x(xkb_context_log_func_table, ptr, log_fn);
    xkb_context_set_log_fn(c_context, context_log);
  }
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

static void finalize_scm_xkb_context(SCM context) {
  scm_xkb_context_unref(context);
}

#define FUNC_NAME s_scm_make_xkb_rule_names
SCM_DEFINE_PUBLIC(scm_make_xkb_rule_names, "make-xkb-rule-names", 0, 0, 1,
    (SCM rest),
    "") {
  return scm_make(scm_cons(scm_xkb_rule_names_type, rest));
}
#undef FUNC_NAME

SCM scm_c_make_xkb_rule_names(const char *rules, const char *model,
    const char *layout, const char *variant, const char *options) {
  return scm_make_xkb_rule_names(scm_list_n(
        k_rules,   rules   ? scm_from_utf8_string(rules)   : SCM_BOOL_F,
        k_model,   model   ? scm_from_utf8_string(model)   : SCM_BOOL_F,
        k_layout,  layout  ? scm_from_utf8_string(layout)  : SCM_BOOL_F,
        k_variant, variant ? scm_from_utf8_string(variant) : SCM_BOOL_F,
        k_options, options ? scm_from_utf8_string(options) : SCM_BOOL_F,
        SCM_UNDEFINED));
}

static SCM read_string_var;
static void init_read_string_var(void) {
  read_string_var = scm_c_public_lookup("ice-9 rdelim", "read-string");
}

#define FUNC_NAME s_scm_make_xkb_keymap
SCM_DEFINE_PUBLIC(scm_make_xkb_keymap, "make-xkb-keymap", 1, 0, 1,
    (SCM context, SCM rest),
    "") {
  struct xkb_context *i_context;
  SCM_VALIDATE_XKB_CONTEXT_COPY(SCM_ARG1, context, i_context);
  struct xkb_keymap *keymap;
  scm_dynwind_begin(0);
  SCM input = !scm_is_eq(rest, SCM_EOL) ? SCM_CAR(rest) : SCM_BOOL_F;
  if (scm_is_eq(input, SCM_BOOL_T)) {
    input = scm_current_input_port();
  }
  if (scm_is_keyword(input)) {
    input = scm_make_xkb_rule_names(rest);
  }
  if (scm_is_string(input)) {
    char *string = scm_to_utf8_string(input);
    scm_dynwind_free(string);
    keymap = xkb_keymap_new_from_string(i_context, string,
        XKB_KEYMAP_FORMAT_TEXT_V1,
        XKB_KEYMAP_COMPILE_NO_FLAGS);
  } else if (scm_is_bytevector(input)) {
    keymap = xkb_keymap_new_from_buffer(i_context,
        scm_to_pointer(scm_bytevector_to_pointer(input, scm_from_uint(0))),
        scm_c_bytevector_length(input),
        XKB_KEYMAP_FORMAT_TEXT_V1,
        XKB_KEYMAP_COMPILE_NO_FLAGS);
  } else if (SCM_IS_A_P(input, scm_xkb_rule_names_type)) {
    struct xkb_rule_names names = { NULL, NULL, NULL, NULL, NULL };

#define UNPACK_RULE(_sym) \
    do { \
      SCM _sym = scm_slot_ref(input, scm_from_utf8_symbol(#_sym)); \
      if (!scm_is_false(_sym)) { \
        SCM_VALIDATE_STRING(SCM_ARGn, _sym); \
        names._sym = scm_to_utf8_string(_sym); \
        scm_dynwind_free((void *) names._sym); \
      } \
    } while (0);

    UNPACK_RULE(rules);
    UNPACK_RULE(model);
    UNPACK_RULE(layout);
    UNPACK_RULE(variant);
    UNPACK_RULE(options);

#undef UNPACK_RULE

    keymap = xkb_keymap_new_from_names(i_context, &names,
        XKB_KEYMAP_COMPILE_NO_FLAGS);
  } else if (SCM_OPINFPORTP(input)) {
    int fdes = SCM_FPORT_FDES(input);
    FILE *file = fdopen(fdes, "r");
    keymap = xkb_keymap_new_from_file(i_context, file,
        XKB_KEYMAP_FORMAT_TEXT_V1,
        XKB_KEYMAP_COMPILE_NO_FLAGS);
  } else if (SCM_INPUT_PORT_P(input)) {
    static scm_i_pthread_once_t once = SCM_I_PTHREAD_ONCE_INIT;
    scm_i_pthread_once(&once, init_read_string_var);

    SCM all = scm_call_1(scm_variable_ref(read_string_var), input);
    char *string = scm_to_utf8_string(all);
    scm_dynwind_free(string);
    keymap = xkb_keymap_new_from_string(i_context, string,
        XKB_KEYMAP_FORMAT_TEXT_V1,
        XKB_KEYMAP_COMPILE_NO_FLAGS);
  } else if (scm_is_false(input) || SCM_UNBNDP(input)) {
    keymap = xkb_keymap_new_from_names(i_context, NULL,
        XKB_KEYMAP_COMPILE_NO_FLAGS);
  } else {
    SCM_ASSERT_TYPE(0, input, SCM_ARG2, FUNC_NAME,
        "<xkb-rule-names>, string, bytevector, #t, or #f");
  }
  scm_dynwind_end();
  if (!keymap)
    scm_misc_error(FUNC_NAME, "Failed to create keymap from args ~a",
        scm_list_2(context, input));
  return scm_make_foreign_object_1(scm_xkb_keymap_type, keymap);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keymap_unref
SCM_DEFINE_PUBLIC(scm_xkb_keymap_unref, "xkb-keymap-unref", 1, 0, 0,
    (SCM keymap),
    "") {
  SCM_ASSERT_TYPE(SCM_IS_A_P(keymap, scm_xkb_keymap_type),
      keymap, SCM_ARG1, FUNC_NAME, "<xkb-keymap>");
  struct xkb_keymap *c_keymap = scm_foreign_object_ref(keymap, 0);
  if (c_keymap) {
    xkb_keymap_unref(c_keymap);
    scm_foreign_object_set_x(keymap, 0, NULL);
  }
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keymap_to_string
SCM_DEFINE_PUBLIC(scm_xkb_keymap_to_string,
    "xkb-keymap->string", 1, 0, 0,
    (SCM keymap),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  scm_dynwind_begin(0);
  char *string = xkb_keymap_get_as_string(i_keymap, XKB_KEYMAP_FORMAT_TEXT_V1);
  if (!string)
    scm_report_out_of_memory();
  scm_dynwind_free(string);
  SCM ret = scm_from_utf8_string(string);
  scm_dynwind_end();
  return ret;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keymap_min_keycode
SCM_DEFINE_PUBLIC(scm_xkb_keymap_min_keycode,
    "xkb-keymap-min-keycode", 1, 0, 0,
    (SCM keymap),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  return scm_from_uint32(xkb_keymap_min_keycode(i_keymap));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keymap_max_keycode
SCM_DEFINE_PUBLIC(scm_xkb_keymap_max_keycode,
    "xkb-keymap-max-keycode", 1, 0, 0,
    (SCM keymap),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  return scm_from_uint32(xkb_keymap_max_keycode(i_keymap));
}
#undef FUNC_NAME

static void key_iter(struct xkb_keymap *keymap, xkb_keycode_t key,
                         void *data) {
  (void) keymap;
  SCM proc = SCM_PACK_POINTER(data);
  scm_call_1(proc, scm_from_uint32(key));
}

#define FUNC_NAME s_scm_xkb_keymap_key_for_each
SCM_DEFINE_PUBLIC(scm_xkb_keymap_key_for_each,
    "xkb-keymap-key-for-each", 2, 0, 0,
    (SCM keymap, SCM proc),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  SCM_VALIDATE_PROC(SCM_ARG2, proc);
  xkb_keymap_key_for_each(i_keymap, key_iter, SCM_UNPACK_POINTER(proc));
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keymap_key_get_name
SCM_DEFINE_PUBLIC(scm_xkb_keymap_key_get_name,
    "xkb-keymap-key-get-name", 2, 0, 0,
    (SCM keymap, SCM key),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  const char *name = xkb_keymap_key_get_name(i_keymap, i_key);
  if (!name)
    return SCM_BOOL_F;
  return scm_from_utf8_string(name);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keymap_key_by_name
SCM_DEFINE_PUBLIC(scm_xkb_keymap_key_by_name,
    "xkb-keymap-key-by-name", 2, 0, 0,
    (SCM keymap, SCM name),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  SCM_VALIDATE_STRING(SCM_ARG2, name);
  char *i_name = scm_to_utf8_string(name);
  xkb_keycode_t key = xkb_keymap_key_by_name(i_keymap, i_name);
  free(i_name);
  if (key == XKB_KEYCODE_INVALID)
    return SCM_BOOL_F;
  return scm_from_uint32(key);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keymap_mods
SCM_DEFINE_PUBLIC(scm_xkb_keymap_mods,
    "xkb-keymap-mods", 1, 0, 0,
    (SCM keymap),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  xkb_mod_index_t count = xkb_keymap_num_mods(i_keymap);
  SCM mods = SCM_EOL;
  for (xkb_mod_index_t i = 0; i < count; i++) {
    SCM mod = scm_from_utf8_symbol(
        xkb_keymap_mod_get_name(i_keymap, count - 1 - i));
    mods = scm_cons(mod, mods);
  }
  return mods;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keymap_layouts
SCM_DEFINE_PUBLIC(scm_xkb_keymap_layouts,
    "xkb-keymap-layouts", 1, 0, 0,
    (SCM keymap),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  xkb_layout_index_t count = xkb_keymap_num_layouts(i_keymap);
  SCM layouts = SCM_EOL;
  for (xkb_layout_index_t i = 0; i < count; i++) {
    const char *name = xkb_keymap_layout_get_name(i_keymap, count - 1 - i);
    SCM layout = name ? scm_from_utf8_string(name) : SCM_BOOL_F;
    layouts = scm_cons(layout, layouts);
  }
  return layouts;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keymap_leds
SCM_DEFINE_PUBLIC(scm_xkb_keymap_leds,
    "xkb-keymap-leds", 1, 0, 0,
    (SCM keymap),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  xkb_led_index_t count = xkb_keymap_num_leds(i_keymap);
  SCM leds = SCM_EOL;
  for (xkb_led_index_t i = 0; i < count; i++) {
    SCM led = scm_from_utf8_string(
        xkb_keymap_led_get_name(i_keymap, count - 1 - i));
    leds = scm_cons(led, leds);
  }
  return leds;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keymap_num_layouts_for_key
SCM_DEFINE_PUBLIC(scm_xkb_keymap_num_layouts_for_key,
    "xkb-keymap-num-layouts-for-key", 2, 0, 0,
    (SCM keymap, SCM key),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  return scm_from_uint32(xkb_keymap_num_layouts_for_key(i_keymap, i_key));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keymap_num_levels_for_key
SCM_DEFINE_PUBLIC(scm_xkb_keymap_num_levels_for_key,
    "xkb-keymap-num-levels-for-key", 3, 0, 0,
    (SCM keymap, SCM key, SCM layout),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  xkb_layout_index_t i_layout;
  SCM_VALIDATE_UINT_COPY(SCM_ARG3, layout, i_layout);
  return scm_from_uint32(
      xkb_keymap_num_levels_for_key(i_keymap, i_key, i_layout));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keymap_key_get_mods_for_level
SCM_DEFINE_PUBLIC(scm_xkb_keymap_key_get_mods_for_level,
    "xkb-keymap-key-get-mods-for-level", 4, 0, 0,
    (SCM keymap, SCM key, SCM layout, SCM level),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  xkb_layout_index_t i_layout;
  SCM_VALIDATE_UINT_COPY(SCM_ARG3, layout, i_layout);
  xkb_level_index_t i_level;
  SCM_VALIDATE_UINT_COPY(SCM_ARG4, level, i_level);
  int count;
  const xkb_keysym_t *syms;
  size_t upper = 128;
  xkb_mod_mask_t *mods = scm_malloc(sizeof(xkb_mod_mask_t) * upper);
  for (;;) {
    count
      = xkb_keymap_key_get_mods_for_level(i_keymap, i_key, i_layout, i_level,
                                          mods, upper);
    if (count < upper)
      break;
    upper *= 2;
    if (upper == 0) {
      free(mods);
      errno = EOVERFLOW;
      scm_syserror(FUNC_NAME);
    }
    mods = scm_realloc(mods, sizeof(xkb_mod_mask_t) * upper);
  }
  return scm_take_u32vector(mods, count);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keymap_key_get_syms_by_level
SCM_DEFINE_PUBLIC(scm_xkb_keymap_key_get_syms_by_level,
    "xkb-keymap-key-get-syms-by-level", 4, 0, 0,
    (SCM keymap, SCM key, SCM layout, SCM level),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  xkb_layout_index_t i_layout;
  SCM_VALIDATE_UINT_COPY(SCM_ARG3, layout, i_layout);
  xkb_level_index_t i_level;
  SCM_VALIDATE_UINT_COPY(SCM_ARG4, level, i_level);
  int count;
  const xkb_keysym_t *syms;
  count = xkb_keymap_key_get_syms_by_level(i_keymap, i_key, i_layout, i_level, &syms);
  SCM ret = scm_make_u32vector(scm_from_int(count), SCM_UNDEFINED);
  scm_t_array_handle handle;
  uint32_t *buf = scm_u32vector_writable_elements(ret, &handle, NULL, NULL);
  memcpy(buf, syms, count * sizeof(xkb_keysym_t));
  scm_array_handle_release(&handle);
  return ret;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_keymap_key_repeats
SCM_DEFINE_PUBLIC(scm_xkb_keymap_key_repeats,
    "xkb-keymap-key-repeats?", 2, 0, 0,
    (SCM keymap, SCM key),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  return scm_from_bool(xkb_keymap_key_repeats(i_keymap, i_key));
}
#undef FUNC_NAME

static void finalize_scm_xkb_keymap(SCM keymap) {
  scm_xkb_keymap_unref(keymap);
}

#define FUNC_NAME s_scm_make_xkb_state
SCM_DEFINE_PUBLIC(scm_make_xkb_state, "make-xkb-state", 1, 0, 0,
    (SCM keymap),
    "") {
  struct xkb_keymap *i_keymap;
  SCM_VALIDATE_XKB_KEYMAP_COPY(SCM_ARG1, keymap, i_keymap);
  struct xkb_state *state = xkb_state_new(i_keymap);
  if (!state)
    scm_report_out_of_memory();
  return scm_make_foreign_object_1(scm_xkb_state_type, state);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_unref
SCM_DEFINE_PUBLIC(scm_xkb_state_unref, "xkb-state-unref", 1, 0, 0,
    (SCM state),
    "") {
  SCM_ASSERT_TYPE(SCM_IS_A_P(state, scm_xkb_state_type),
      state, SCM_ARG1, FUNC_NAME, "<xkb-state>");
  struct xkb_state *c_state = scm_foreign_object_ref(state, 0);
  if (c_state) {
    xkb_state_unref(c_state);
    scm_foreign_object_set_x(state, 0, NULL);
  }
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_update_key
SCM_DEFINE_PUBLIC(scm_xkb_state_update_key,
    "xkb-state-update-key", 3, 0, 0,
    (SCM state, SCM key, SCM pressed),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  bool i_pressed;
  SCM_VALIDATE_BOOL_COPY(SCM_ARG3, pressed, i_pressed);
  return scm_from_uint(xkb_state_update_key(i_state, i_key,
        i_pressed ?  XKB_KEY_DOWN : XKB_KEY_UP));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_update_mask
SCM_DEFINE_PUBLIC(scm_xkb_state_update_mask,
    "xkb-state-update-mask", 7, 0, 0,
    (SCM state, SCM depressed_mods, SCM latched_mods, SCM locked_mods,
     SCM depressed_layout, SCM latched_layout, SCM locked_layout),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  xkb_mod_mask_t i_depressed_mods;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, depressed_mods, i_depressed_mods);
  xkb_mod_mask_t i_latched_mods;
  SCM_VALIDATE_UINT_COPY(SCM_ARG3, latched_mods, i_latched_mods);
  xkb_mod_mask_t i_locked_mods;
  SCM_VALIDATE_UINT_COPY(SCM_ARG4, locked_mods, i_locked_mods);
  xkb_layout_index_t i_depressed_layout;
  SCM_VALIDATE_UINT_COPY(SCM_ARG5, depressed_layout, i_depressed_layout);
  xkb_layout_index_t i_latched_layout;
  SCM_VALIDATE_UINT_COPY(SCM_ARG6, latched_layout, i_latched_layout);
  xkb_layout_index_t i_locked_layout;
  SCM_VALIDATE_UINT_COPY(SCM_ARG7, locked_layout, i_locked_layout);
  return scm_from_uint(xkb_state_update_mask(i_state,
        i_depressed_mods, i_latched_mods, i_locked_mods,
        i_depressed_layout, i_latched_layout, i_locked_layout));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_key_get_syms
SCM_DEFINE_PUBLIC(scm_xkb_state_key_get_syms,
    "xkb-state-key-get-syms", 2, 0, 0,
    (SCM state, SCM key),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  const xkb_keysym_t *syms;
  int count = xkb_state_key_get_syms(i_state, i_key, &syms);
  SCM keysyms = SCM_EOL;
  for (int i = 0; i < count; i++) {
    keysyms = scm_cons(scm_from_uint32(syms[count - 1 - i]), keysyms);
  }
  return keysyms;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_key_get_string
SCM_DEFINE_PUBLIC(scm_xkb_state_key_get_string,
    "xkb-state-key-get-string", 2, 0, 0,
    (SCM state, SCM key),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  char buffer[128];
  int size = xkb_state_key_get_utf8(i_state, i_key, buffer, sizeof(buffer));
  if (size < 0)
    return SCM_BOOL_F;
  return scm_from_utf8_stringn(buffer, size);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_key_get_char
SCM_DEFINE_PUBLIC(scm_xkb_state_key_get_char,
    "xkb-state-key-get-char", 2, 0, 0,
    (SCM state, SCM key),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  uint32_t c = xkb_state_key_get_utf32(i_state, i_key);
  if (!c)
    return SCM_BOOL_F;
  return scm_c_make_char(c);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_key_get_one_sym
SCM_DEFINE_PUBLIC(scm_xkb_state_key_get_one_sym,
    "xkb-state-key-get-one-sym", 2, 0, 0,
    (SCM state, SCM key),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  xkb_keysym_t sym = xkb_state_key_get_one_sym(i_state, i_key);
  if (sym == XKB_KEY_NoSymbol)
    return SCM_BOOL_F;
  return scm_from_uint32(sym);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_key_get_layout
SCM_DEFINE_PUBLIC(scm_xkb_state_key_get_layout,
    "xkb-state-key-get-layout", 2, 0, 0,
    (SCM state, SCM key),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  xkb_layout_index_t layout = xkb_state_key_get_layout(i_state, i_key);
  if (layout == XKB_LAYOUT_INVALID)
    return SCM_BOOL_F;
  return scm_from_uint32(layout);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_key_get_level
SCM_DEFINE_PUBLIC(scm_xkb_state_key_get_level,
    "xkb-state-key-get-level", 3, 0, 0,
    (SCM state, SCM key, SCM layout),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  xkb_layout_index_t i_layout;
  SCM_VALIDATE_UINT_COPY(SCM_ARG3, layout, i_layout);
  xkb_level_index_t level = xkb_state_key_get_level(i_state, i_key, i_layout);
  if (level == XKB_LEVEL_INVALID)
    return SCM_BOOL_F;
  return scm_from_uint32(level);
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_serialize_mods
SCM_DEFINE_PUBLIC(scm_xkb_state_serialize_mods,
    "xkb-state-serialize-mods", 2, 0, 0,
    (SCM state, SCM components),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  enum xkb_state_component i_components;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, components, i_components);
  return scm_from_uint32(xkb_state_serialize_mods(i_state, i_components));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_serialize_layout
SCM_DEFINE_PUBLIC(scm_xkb_state_serialize_layout,
    "xkb-state-serialize-layout", 2, 0, 0,
    (SCM state, SCM components),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  enum xkb_state_component i_components;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, components, i_components);
  return scm_from_uint32(xkb_state_serialize_layout(i_state, i_components));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_mod_active_p
SCM_DEFINE_PUBLIC(scm_xkb_state_mod_active_p,
    "xkb-state-mod-active?", 3, 0, 0,
    (SCM state, SCM mod, SCM type),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  enum xkb_state_component i_type;
  SCM_VALIDATE_UINT_COPY(SCM_ARG3, type, i_type);
  int ret;
  if (scm_is_symbol(mod)) {
    mod = scm_symbol_to_string(mod);
  }
  if (scm_is_integer(mod)) {
    ret = xkb_state_mod_index_is_active(i_state, scm_to_uint(mod), i_type);
  } else if (scm_is_string(mod)) {
    char *name = scm_to_utf8_string(mod);
    ret = xkb_state_mod_name_is_active(i_state, name, i_type);
    free(name);
  } else {
    SCM_ASSERT_TYPE(0, mod, SCM_ARG3, FUNC_NAME, "string, symbol or integer");
  }
  if (ret == 0)
    return SCM_BOOL_F;
  if (ret == -1)
    return SCM_ELISP_NIL;
  return SCM_BOOL_T;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_key_get_consumed_mods
SCM_DEFINE_PUBLIC(scm_xkb_state_key_get_consumed_mods,
    "xkb-state-key-get-consumed-mods", 2, 0, 1,
    (SCM state, SCM key, SCM rest),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  SCM gtk = SCM_BOOL_F;

  scm_c_bind_keyword_arguments(FUNC_NAME, rest, 0, k_gtk, &gtk, SCM_UNDEFINED);
  enum xkb_consumed_mode mode
    = scm_is_false(gtk) ? XKB_CONSUMED_MODE_XKB : XKB_CONSUMED_MODE_GTK;
  return scm_from_uint32(
      xkb_state_key_get_consumed_mods2(i_state, i_key, mode));
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_mod_consumed_p
SCM_DEFINE_PUBLIC(scm_xkb_state_mod_consumed_p,
    "xkb-state-mod-consumed?", 3, 0, 1,
    (SCM state, SCM key, SCM index, SCM rest),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  xkb_keycode_t i_key;
  SCM_VALIDATE_UINT_COPY(SCM_ARG2, key, i_key);
  xkb_mod_index_t i_index;
  SCM_VALIDATE_UINT_COPY(SCM_ARG3, index, i_index);
  SCM gtk = SCM_BOOL_F;

  scm_c_bind_keyword_arguments(FUNC_NAME, rest, 0, k_gtk, &gtk, SCM_UNDEFINED);
  enum xkb_consumed_mode mode
    = scm_is_false(gtk) ? XKB_CONSUMED_MODE_XKB : XKB_CONSUMED_MODE_GTK;
  int ret = xkb_state_mod_index_is_consumed2(i_state, i_key, i_index, mode);
  if (ret == 0)
    return SCM_BOOL_F;
  if (ret == -1)
    return SCM_ELISP_NIL;
  return SCM_BOOL_T;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_layout_active_p
SCM_DEFINE_PUBLIC(scm_xkb_state_layout_active_p,
    "xkb-state-layout-active?", 3, 0, 0,
    (SCM state, SCM layout, SCM type),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  SCM_ASSERT_TYPE(scm_is_integer(layout) || scm_is_string(layout),
      layout, SCM_ARG2, FUNC_NAME, "string or integer");
  enum xkb_state_component i_type;
  SCM_VALIDATE_UINT_COPY(SCM_ARG3, type, i_type);
  int ret;
  if (scm_is_integer(layout)) {
    ret = xkb_state_layout_index_is_active(i_state, scm_to_uint(layout), i_type);
  } else {
    char *name = scm_to_utf8_string(layout);
    ret = xkb_state_layout_name_is_active(i_state, name, i_type);
    free(name);
  }
  if (ret == 0)
    return SCM_BOOL_F;
  if (ret == -1)
    return SCM_ELISP_NIL;
  return SCM_BOOL_T;
}
#undef FUNC_NAME

#define FUNC_NAME s_scm_xkb_state_led_active_p
SCM_DEFINE_PUBLIC(scm_xkb_state_led_active_p,
    "xkb-state-led-active?", 2, 0, 0,
    (SCM state, SCM led),
    "") {
  struct xkb_state *i_state;
  SCM_VALIDATE_XKB_STATE_COPY(SCM_ARG1, state, i_state);
  SCM_ASSERT_TYPE(scm_is_integer(led) || scm_is_string(led),
      led, SCM_ARG2, FUNC_NAME, "string or integer");
  int ret;
  if (scm_is_integer(led)) {
    ret = xkb_state_led_index_is_active(i_state, scm_to_uint(led));
  } else {
    char *name = scm_to_utf8_string(led);
    ret = xkb_state_led_name_is_active(i_state, name);
    free(name);
  }
  if (ret == 0)
    return SCM_BOOL_F;
  if (ret == -1)
    return SCM_ELISP_NIL;
  return SCM_BOOL_T;
}
#undef FUNC_NAME

static void finalize_scm_xkb_state(SCM state) {
  scm_xkb_state_unref(state);
}

#define SCM_PUBLIC_ENUM(name) \
  do { \
    scm_c_define(#name, scm_from_uint(name)); \
    scm_c_export(#name, NULL); \
  } while (0)

void scm_init_xkbcommon(void) {
  xkb_context_log_func_table = scm_make_weak_key_hash_table(SCM_INUM0);

  k_case_insensitive = scm_from_utf8_keyword("case-insensitive?");
  k_no_default_includes = scm_from_utf8_keyword("no-default-includes?");
  k_no_environment_names = scm_from_utf8_keyword("no-environment-names?");
  k_gtk = scm_from_utf8_keyword("gtk");

  SCM make_class
    = scm_variable_ref(scm_c_public_lookup("oop goops", "make-class"));
  SCM k_init_value   = scm_from_utf8_keyword("init-value");
  SCM k_init_keyword = scm_from_utf8_keyword("init-keyword");

#define RW_SLOT(_name) ({ \
    sym_##_name   = scm_from_utf8_symbol(#_name); \
    k_##_name = scm_symbol_to_keyword(sym_##_name); \
    scm_list_n(sym_##_name, \
        k_init_value, SCM_BOOL_F, \
        k_init_keyword, k_##_name, \
        SCM_UNDEFINED); \
    })

  static const char s_scm_xkb_rule_names[] = "<xkb-rule-names>";
  scm_xkb_rule_names_type = scm_call_4(make_class, SCM_EOL,
      scm_list_5(RW_SLOT(rules), RW_SLOT(model), RW_SLOT(layout),
        RW_SLOT(variant), RW_SLOT(options)),
      scm_from_utf8_keyword("name"),
      scm_from_utf8_symbol(s_scm_xkb_rule_names));

#undef RW_SLOT

  scm_c_define(s_scm_xkb_rule_names, scm_xkb_rule_names_type);
  scm_c_export(s_scm_xkb_rule_names, NULL);

  static const char s_scm_xkb_context[] = "<xkb-context>";
  scm_xkb_context_type = scm_make_foreign_object_type(
      scm_from_utf8_symbol(s_scm_xkb_context),
      scm_list_1(scm_from_utf8_symbol("context")),
      finalize_scm_xkb_context);
  scm_c_define(s_scm_xkb_context, scm_xkb_context_type);
  scm_c_export(s_scm_xkb_context, NULL);

  static const char s_scm_xkb_keymap[] = "<xkb-keymap>";
  scm_xkb_keymap_type = scm_make_foreign_object_type(
      scm_from_utf8_symbol(s_scm_xkb_keymap),
      scm_list_1(scm_from_utf8_symbol("keymap")),
      finalize_scm_xkb_keymap);
  scm_c_define(s_scm_xkb_keymap, scm_xkb_keymap_type);
  scm_c_export(s_scm_xkb_keymap, NULL);

  static const char s_scm_xkb_state[] = "<xkb-state>";
  scm_xkb_state_type = scm_make_foreign_object_type(
      scm_from_utf8_symbol(s_scm_xkb_state),
      scm_list_1(scm_from_utf8_symbol("state")),
      finalize_scm_xkb_state);
  scm_c_define(s_scm_xkb_state, scm_xkb_state_type);
  scm_c_export(s_scm_xkb_state, NULL);

  SCM_PUBLIC_ENUM(XKB_LOG_LEVEL_CRITICAL);
  SCM_PUBLIC_ENUM(XKB_LOG_LEVEL_ERROR);
  SCM_PUBLIC_ENUM(XKB_LOG_LEVEL_WARNING);
  SCM_PUBLIC_ENUM(XKB_LOG_LEVEL_INFO);
  SCM_PUBLIC_ENUM(XKB_LOG_LEVEL_DEBUG);

  SCM_PUBLIC_ENUM(XKB_STATE_MODS_DEPRESSED);
  SCM_PUBLIC_ENUM(XKB_STATE_MODS_LATCHED);
  SCM_PUBLIC_ENUM(XKB_STATE_MODS_LOCKED);
  SCM_PUBLIC_ENUM(XKB_STATE_MODS_EFFECTIVE);
  SCM_PUBLIC_ENUM(XKB_STATE_LAYOUT_DEPRESSED);
  SCM_PUBLIC_ENUM(XKB_STATE_LAYOUT_LATCHED);
  SCM_PUBLIC_ENUM(XKB_STATE_LAYOUT_LOCKED);
  SCM_PUBLIC_ENUM(XKB_STATE_LAYOUT_EFFECTIVE);
  SCM_PUBLIC_ENUM(XKB_STATE_LEDS);

#ifndef SCM_MAGIC_SNARFER
#include "guile-xkbcommon.x"
#include "names.i"
#endif
}

void scm_init_xkbcommon_keysyms(void) {
#ifndef SCM_MAGIC_SNARFER
#include "keysyms.i"
#endif
}
