;;;; -*- coding: utf-8; mode: scheme -*-
;;;; SPDX-FileCopyrightText: 2020 Jason Francis <jason@cycles.network>
;;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (xkbcommon tests keymap)
  #:use-module (xkbcommon)
  #:use-module (ice-9 iconv)
  #:use-module (ice-9 textual-ports)
  #:use-module (oop goops)
  #:use-module (srfi srfi-64)
  #:duplicates (merge-generics))

(module-set! (resolve-module '(srfi srfi-64)) 'test-log-to-file #f)
(test-begin "xkbcommon")

(define ctx (make <xkb-context> #:no-environment-names? #t))
(define keymap-file (string-append (dirname (current-filename)) "/empty.xkb"))
(define keymap-string (call-with-input-file keymap-file
                        (λ (port) (get-string-all port))))

(define (make-test-rmlvo)
  (make <xkb-rule-names>
        #:rules "evdev"
        #:model "pc101"
        #:layout "us"
        #:variant "intl"
        #:options "grp:shift_toggle"))

(let ((rmlvo (make-test-rmlvo)))
  (test-group "<xkb-rule-names> rules"
    (test-equal (rules rmlvo) "evdev")
    (set! (rules rmlvo) "xorg")
    (test-equal (rules rmlvo) "xorg"))
  (test-group "<xkb-rule-names> model"
    (test-equal (model rmlvo) "pc101")
    (set! (model rmlvo) "pc102")
    (test-equal (model rmlvo) "pc102"))
  (test-group "<xkb-rule-names> layout"
    (test-equal (layout rmlvo) "us")
    (set! (layout rmlvo) "de")
    (test-equal (layout rmlvo) "de"))
  (test-group "<xkb-rule-names> variant"
    (test-equal (variant rmlvo) "intl")
    (set! (variant rmlvo) "dvorak")
    (test-equal (variant rmlvo) "dvorak"))
  (test-group "<xkb-rule-names> options"
    (test-equal (options rmlvo) "grp:shift_toggle")
    (set! (options rmlvo) "altwin:menu")
    (test-equal (options rmlvo) "altwin:menu")))

(test-group "default constructor"
  (test-assert (make <xkb-keymap> ctx)))

(test-group "unref"
  (test-assert (begin (xkb-keymap-unref (make <xkb-keymap> ctx)) #t)))

(test-group "->string"
  (let ((km (make <xkb-keymap> ctx)))
    (test-assert (string-prefix? "xkb_keymap" (->string km)))))

(test-group "string constructor"
  (test-assert (make <xkb-keymap> ctx keymap-string)))

(test-group "invalid string constructor"
  (test-error (make <xkb-keymap> ctx "xkb_bad_keymap []")))

(test-group "bytevector constructor"
  (test-assert
    (make <xkb-keymap> ctx (string->bytevector keymap-string "utf-8"))))

(test-group "invalid bytevector constructor"
  (test-error (make <xkb-keymap> ctx #u8(99 128 88 92))))

(test-group "port constructor"
  (test-assert
    (call-with-input-string keymap-string
      (λ (port) (make <xkb-keymap> ctx port)))))

(test-group "file port constructor"
  (test-assert
    (call-with-input-file keymap-file
      (λ (port) (make <xkb-keymap> ctx port)))))

(test-group "#t constructor"
  (test-equal
    (with-input-from-file keymap-file
      (λ () (->string (make <xkb-keymap> ctx #t))))
    (->string (make <xkb-keymap> ctx keymap-string))))

(test-group "empty <xkb-rule-names> constructor"
  (test-assert (make <xkb-keymap> ctx (make <xkb-rule-names>))))

(test-group "<xkb-rule-names> constructor"
  (test-assert (make <xkb-keymap> ctx (make-test-rmlvo))))

(test-group "keyword constructor"
  (test-assert (make <xkb-keymap> ctx #:layout "fr")))

(test-group "bad layout constructor"
  (test-error (make <xkb-keymap> ctx #:layout "BADLAYOUT")))

(test-group "bad keyword constructor"
  (test-error (make <xkb-keymap> ctx #:badkeyword "BAD")))

(let ((km (make <xkb-keymap> ctx keymap-string)))
  (test-group "min-keycode"
    (test-equal 8 (min-keycode km)))
  (test-group "max-keycode"
    (test-equal 255 (max-keycode km)))
  (test-group "key-for-each"
    (let ((count 0)
          (maximum 0))
      (key-for-each km (λ (key)
                         (set! count (1+ count))
                         (set! maximum (max key maximum))))
    (test-equal count 248)
    (test-equal maximum 255)))
  (test-group "key-map"
    (let* ((maximum 0)
           (a (key-map km (λ (key)
                            (set! maximum (max key maximum))
                            #f))))
    (test-equal (length a) 248)
    (test-equal (length (filter not a)) 248)
    (test-equal maximum 255)))
  (test-group "keys"
    (test-equal 248 (length (keys km))))
  (test-group "invalid key-get-name"
    (test-equal #f (key-get-name km 32)))
  (test-group "invalid key-by-name"
    (test-equal #f (key-by-name km "bad-key")))
  (test-group "mods"
    (test-equal 8 (length (mods km))))
  (test-group "mod-flag"
    (test-equal 4 (mod-flag km XKB_MOD_NAME_CTRL)))
  (test-group "invalid mod-flag"
    (test-equal #f (mod-flag km 'BADMOD)))
  (test-group "mod-flags"
    (let ((flags (mod-flags km)))
      (test-equal 8 (length flags))
      (test-equal (assoc-ref flags XKB_MOD_NAME_CTRL)
                  (mod-flag km XKB_MOD_NAME_CTRL))))
  (test-group "mod-index"
    (test-equal 2 (mod-index km XKB_MOD_NAME_CTRL)))
  (test-group "mod-index name"
    (test-equal 2 (mod-index km XKB_MOD_NAME_CTRL)))
  (test-group "empty layouts"
    (test-equal '() (layouts km)))
  (test-group "empty leds"
    (test-equal '() (leds km)))
  (test-group "keys"
    (test-equal 248 (length (keys km)))))

(let ((km (make <xkb-keymap> ctx #:rules "evdev" #:model "pc101"
                                 #:layout "us"   #:options "")))
  (test-group "layouts"
    (test-equal '("English (US)") (layouts km)))
  (test-group "layout-index"
    (test-equal 0 (layout-index km "English (US)")))
  (test-group "invalid layout-index"
    (test-equal #f (layout-index km "BADLAYOUT")))
  (test-group "leds"
    (test-assert (member XKB_LED_NAME_CAPS (leds km))))
  (test-group "led-index"
    (test-equal 0 (led-index km XKB_LED_NAME_CAPS)))
  (test-group "invalid led-index"
    (test-equal #f (led-index km "BAD LED")))
  (test-group "key-get-name"
    (test-equal (key-get-name km 9) "ESC"))
  (test-group "key-by-name"
    (test-equal (key-by-name km "ESC") 9))
  (test-group "num-layouts-for-key"
    (test-equal (num-layouts-for-key km 9) 1))
  (test-group "invalid num-layouts-for-key"
    (test-equal (num-layouts-for-key km 93) 0))
  (test-group "num-levels-for-key"
    (test-equal (num-levels-for-key km 9) 1))
  (test-group "invalid num-levels-for-key"
    (test-equal (num-levels-for-key km 93) 0))
  (test-group "key-get-mods-for-level"
    (test-equal #u32(0 17) (key-get-mods-for-level km 79 0 0)))
  (test-group "invalid key-get-mods-for-level"
    (test-equal #u32() (key-get-mods-for-level km 93 0 0)))
  (test-group "key-get-syms-by-level"
    (test-equal #u32(65307) (key-get-syms-by-level km 9 0 0)))
  (test-group "invalid key-get-syms-by-level"
    (test-equal #u32() (key-get-syms-by-level km 93 0 0)))
  (test-group "key-repeats?"
    (test-assert (key-repeats? km 9))
    (test-equal #f (key-repeats? km 93))))

(let ((km (make <xkb-keymap> ctx #:layout "us,de,fr")))
  (test-group "multiple layouts"
    (test-equal 3 (length (layouts km)))))

(let ((km (make <xkb-keymap> ctx #:layout #f)))
  (test-group "#f layout"
    (test-equal 1 (length (layouts km)))))

(test-group-with-cleanup "ignoring layout without environment names"
  (setenv "XKB_DEFAULT_LAYOUT" "BADLAYOUT")
  (test-assert (make <xkb-keymap> ctx))
  (unsetenv "XKB_DEFAULT_LAYOUT"))

(test-group-with-cleanup "layout from environment"
  (setenv "XKB_DEFAULT_LAYOUT" "ro,de")
  (let ((km (make <xkb-keymap> (make <xkb-context>))))
    (test-equal 2 (length (layouts km))))
  (unsetenv "XKB_DEFAULT_LAYOUT"))

(test-group-with-cleanup "bad layout from environment"
  (setenv "XKB_DEFAULT_LAYOUT" "BADLAYOUT")
  (test-error (make <xkb-keymap> (make <xkb-context>)))
  (unsetenv "XKB_DEFAULT_LAYOUT"))

(exit (if (zero? (test-runner-fail-count (test-end))) 0 1))

