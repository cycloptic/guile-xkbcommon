;;;; -*- coding: utf-8; mode: scheme -*-
;;;; SPDX-FileCopyrightText: 2020 Jason Francis <jason@cycles.network>
;;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (xkbcommon tests state)
  #:use-module (xkbcommon)
  #:use-module (oop goops)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-64)
  #:duplicates (merge-generics replace))

(module-set! (resolve-module '(srfi srfi-64)) 'test-log-to-file #f)
(test-begin "xkbcommon")

(define ctx (make <xkb-context>))
(define km (make <xkb-keymap> ctx #:rules "evdev"  #:model "pc101"
                                  #:layout "us,de" #:options ""))

(test-group "constructor"
  (test-assert (make <xkb-state> km)))

(test-group "unref"
  (test-assert (begin (xkb-state-unref (make <xkb-state> km)) #t)))

(define mod-pressed-mask (logior XKB_STATE_MODS_DEPRESSED
                                 XKB_STATE_MODS_EFFECTIVE))
(define layout-pressed-mask (logior XKB_STATE_LAYOUT_DEPRESSED
                                    XKB_STATE_LAYOUT_EFFECTIVE
                                    XKB_STATE_LEDS))
(define led-pressed-mask (logior XKB_STATE_MODS_LOCKED
                                 XKB_STATE_MODS_EFFECTIVE
                                 XKB_STATE_LEDS))

(test-group "update-key"
  (let ((state (make <xkb-state> km)))
    (test-equal 0 (update-key state 9 #t))
    (test-equal 0 (update-key state 9 #f))))

(test-group "update-key mod"
  (let ((state (make <xkb-state> km)))
    (test-equal mod-pressed-mask (update-key state 50 #t))
    (test-equal mod-pressed-mask (update-key state 50 #f))))

(test-group "update-key mod twice"
  (let ((state (make <xkb-state> km)))
    (test-equal mod-pressed-mask (update-key state 50 #t))
    (test-equal 0 (update-key state 50 #t))
    (test-equal 0 (update-key state 50 #f))
    (test-equal mod-pressed-mask (update-key state 50 #f))))

(test-group "update-mask empty"
  (let ((state (make <xkb-state> km)))
    (test-equal 0 (update-mask state 0 0 0 0 0 0))))

(test-group "update-mask mod"
  (let ((state (make <xkb-state> km))
        (ctrl (mod-flag km XKB_MOD_NAME_CTRL)))
    (test-equal mod-pressed-mask (update-mask state ctrl 0 0 0 0 0))
    (test-equal mod-pressed-mask (update-mask state 0 0 0 0 0 0))))

(test-group "update-mask mod twice"
  (let ((state (make <xkb-state> km))
        (ctrl (mod-flag km XKB_MOD_NAME_CTRL)))
    (test-equal mod-pressed-mask (update-mask state ctrl 0 0 0 0 0))
    (test-equal 0 (update-mask state ctrl 0 0 0 0 0))
    (test-equal mod-pressed-mask (update-mask state 0 0 0 0 0 0))))

(test-group "update-mask layout"
  (let ((state (make <xkb-state> km))
        (de (layout-index km "German")))
    (test-equal layout-pressed-mask (update-mask state 0 0 0 de 0 0))
    (test-equal layout-pressed-mask (update-mask state 0 0 0 0 0 0))))

(test-group "update-mask layout twice"
  (let ((state (make <xkb-state> km))
        (de (layout-index km "German")))
    (test-equal layout-pressed-mask (update-mask state 0 0 0 de 0 0))
    (test-equal 0 (update-mask state 0 0 0 de 0 0))
    (test-equal layout-pressed-mask (update-mask state 0 0 0 0 0 0))))

(let ((state (make <xkb-state> km)))
  (test-group "key-get-syms"
    (test-equal '(65307) (key-get-syms state 9)))
  (test-group "key-get-string"
    (test-equal "\x1b" (key-get-string state 9)))
  (test-group "key-get-string letter"
    (test-equal "x" (key-get-string state 53)))
  (test-group "key-get-char"
    (test-equal #\esc (key-get-char state 9)))
  (test-group "key-get-one-sym"
    (test-equal 65307 (key-get-one-sym state 9)))
  (test-group "key-get-layout"
    (test-equal 0 (key-get-layout state 9)))
  (test-group "key-get-level"
    (test-equal 0 (key-get-level state 9 0)))
  (test-group "invalid key-get-syms"
    (test-equal '() (key-get-syms state 93)))
  (test-group "invalid key-get-string"
    (test-equal "" (key-get-string state 93)))
  (test-group "invalid key-get-char"
    (test-equal #f (key-get-char state 93)))
  (test-group "invalid key-get-one-sym"
    (test-equal #f (key-get-one-sym state 93)))
  (test-group "invalid key-get-layout"
    (test-equal #f (key-get-layout state 93)))
  (test-group "invalid key-get-level"
    (test-equal #f (key-get-level state 93 0))))

(test-group "serialize-mods"
  (let ((state (make <xkb-state> km))
        (ctrl (mod-flag km XKB_MOD_NAME_CTRL)))
    (test-equal 0 (serialize-mods state mod-pressed-mask))
    (test-equal mod-pressed-mask (update-mask state ctrl 0 0 0 0 0))
    (test-equal ctrl (serialize-mods state mod-pressed-mask))
    (test-equal mod-pressed-mask (update-mask state 0 0 0 0 0 0))
    (test-equal 0 (serialize-mods state mod-pressed-mask))))

(test-group "serialize-layout"
  (let ((state (make <xkb-state> km))
        (de (layout-index km "German")))
    (test-equal 0 (serialize-layout state layout-pressed-mask))
    (test-equal layout-pressed-mask (update-mask state 0 0 0 de 0 0))
    (test-equal de (serialize-layout state layout-pressed-mask))
    (test-equal layout-pressed-mask (update-mask state 0 0 0 0 0 0))
    (test-equal 0 (serialize-layout state layout-pressed-mask))))

(test-group "mod-active? invalid"
  (let ((state (make <xkb-state> km)))
    (test-equal #nil (mod-active? state 999 mod-pressed-mask))
    (test-equal #nil (mod-active? state "qwerty" mod-pressed-mask))
    (test-equal #nil (mod-active? state 'qwer mod-pressed-mask))))

(test-group "mod-active? <integer>"
  (let ((state (make <xkb-state> km))
        (ctrl-index (mod-index km XKB_MOD_NAME_CTRL))
        (ctrl (mod-flag km XKB_MOD_NAME_CTRL)))
    (test-equal #f (mod-active? state ctrl-index mod-pressed-mask))
    (test-equal mod-pressed-mask (update-mask state ctrl 0 0 0 0 0))
    (test-equal #t (mod-active? state ctrl-index mod-pressed-mask))
    (test-equal mod-pressed-mask (update-mask state 0 0 0 0 0 0))
    (test-equal #f (mod-active? state ctrl-index mod-pressed-mask))))

(test-group "mod-active? <symbol>"
  (let ((state (make <xkb-state> km))
        (ctrl (mod-flag km XKB_MOD_NAME_CTRL)))
    (test-equal #f (mod-active? state XKB_MOD_NAME_CTRL mod-pressed-mask))
    (test-equal mod-pressed-mask (update-mask state ctrl 0 0 0 0 0))
    (test-equal #t (mod-active? state XKB_MOD_NAME_CTRL mod-pressed-mask))
    (test-equal mod-pressed-mask (update-mask state 0 0 0 0 0 0))
    (test-equal #f (mod-active? state XKB_MOD_NAME_CTRL mod-pressed-mask))))

(test-group "mod-active? <string>"
  (let ((state (make <xkb-state> km))
        (ctrl (mod-flag km XKB_MOD_NAME_CTRL)))
    (test-equal #f (mod-active? state "Control" mod-pressed-mask))
    (test-equal mod-pressed-mask (update-mask state ctrl 0 0 0 0 0))
    (test-equal #t (mod-active? state "Control" mod-pressed-mask))
    (test-equal mod-pressed-mask (update-mask state 0 0 0 0 0 0))
    (test-equal #f (mod-active? state "Control" mod-pressed-mask))))

(let* ((state (make <xkb-state> km))
       (ks (keys km))
       (z (find (λ (key) (equal? (key-get-char state key) #\z)) ks))
       (star (find (λ (key) (equal? (key-get-char state key) #\*)) ks))
       (shift-index (mod-index km XKB_MOD_NAME_SHIFT))
       (logo-index (mod-index km XKB_MOD_NAME_LOGO)))
  (test-group "key-get-consumed-mods"
    (test-assert z)
    (test-assert star)
    (test-equal (logior XKB_STATE_MODS_DEPRESSED
                        XKB_STATE_MODS_LATCHED)
                (key-get-consumed-mods state z)))
    (test-equal (logior XKB_STATE_MODS_DEPRESSED
                        XKB_STATE_MODS_LOCKED
                        XKB_STATE_MODS_EFFECTIVE
                        XKB_STATE_LAYOUT_EFFECTIVE)
                (key-get-consumed-mods state star))

  (test-group "key-get-consumed-mods #:gtk"
    (test-equal (logior XKB_STATE_MODS_DEPRESSED
                        XKB_STATE_MODS_LATCHED)
                (key-get-consumed-mods state z #:gtk #t))
    (test-equal 0 (key-get-consumed-mods state star #:gtk #t)))

  (test-group "mod-consumed?"
    (test-equal #t (mod-consumed? state z shift-index))
    (test-equal #t (mod-consumed? state star shift-index))
    (test-equal #f (mod-consumed? state z logo-index))
    (test-equal #f (mod-consumed? state star logo-index)))

  (test-group "mod-consumed? #:gtk"
    (test-equal #t (mod-consumed? state z shift-index #:gtk #t))
    (test-equal #f (mod-consumed? state star shift-index #:gtk #t))
    (test-equal #f (mod-consumed? state z logo-index #:gtk #t))
    (test-equal #f (mod-consumed? state star logo-index #:gtk #t))))

(test-group "layout-active? invalid"
  (let ((state (make <xkb-state> km)))
    (test-equal #nil (layout-active? state 999 layout-pressed-mask))
    (test-equal #nil (layout-active? state "BADLAYOUT" layout-pressed-mask))))

(test-group "layout-active? <integer>"
  (let ((state (make <xkb-state> km))
        (de (layout-index km "German")))
    (test-equal #t (layout-active? state 0 layout-pressed-mask))
    (test-equal #f (layout-active? state de layout-pressed-mask))
    (test-equal layout-pressed-mask (update-mask state 0 0 0 de 0 0))
    (test-equal #t (layout-active? state de layout-pressed-mask))
    (test-equal layout-pressed-mask (update-mask state 0 0 0 0 0 0))
    (test-equal #f (layout-active? state de layout-pressed-mask))))

(test-group "layout-active? <string>"
  (let ((state (make <xkb-state> km))
        (de (layout-index km "German")))
    (test-equal #t (layout-active? state 0 layout-pressed-mask))
    (test-equal #f (layout-active? state "German" layout-pressed-mask))
    (test-equal layout-pressed-mask (update-mask state 0 0 0 de 0 0))
    (test-equal #t (layout-active? state "German" layout-pressed-mask))
    (test-equal layout-pressed-mask (update-mask state 0 0 0 0 0 0))
    (test-equal #f (layout-active? state "German" layout-pressed-mask))))

(test-group "led-active? invalid"
  (let ((state (make <xkb-state> km)))
    (test-equal #nil (led-active? state 999))
    (test-equal #nil (led-active? state "BAD LED"))))

(test-group "led-active? <integer>"
  (let ((state (make <xkb-state> km))
        (caps-mod (mod-flag km XKB_MOD_NAME_CAPS))
        (caps (led-index km XKB_LED_NAME_CAPS)))
    (test-equal #f (led-active? state caps))
    (test-equal led-pressed-mask (update-mask state 0 0 caps-mod 0 0 0))
    (test-equal #t (led-active? state caps))
    (test-equal led-pressed-mask (update-mask state 0 0 0 0 0 0))
    (test-equal #f (led-active? state caps))))

(test-group "led-active? <string>"
  (let ((state (make <xkb-state> km))
        (caps-mod (mod-flag km XKB_MOD_NAME_CAPS)))
    (test-equal #f (led-active? state XKB_LED_NAME_CAPS))
    (test-equal led-pressed-mask (update-mask state 0 0 caps-mod 0 0 0))
    (test-equal #t (led-active? state XKB_LED_NAME_CAPS))
    (test-equal led-pressed-mask (update-mask state 0 0 0 0 0 0))
    (test-equal #f (led-active? state XKB_LED_NAME_CAPS))))

(exit (if (zero? (test-runner-fail-count (test-end))) 0 1))

