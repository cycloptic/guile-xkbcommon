;;;; -*- coding: utf-8; mode: scheme -*-
;;;; SPDX-FileCopyrightText: 2020 Jason Francis <jason@cycles.network>
;;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (xkbcommon tests context)
  #:use-module (xkbcommon)
  #:use-module (oop goops)
  #:use-module (srfi srfi-64)
  #:duplicates (merge-generics))

(module-set! (resolve-module '(srfi srfi-64)) 'test-log-to-file #f)
(test-begin "xkbcommon")

(test-group "constructor"
  (test-assert (make <xkb-context>)))

(test-group "unref"
  (test-assert (begin (xkb-context-unref (make <xkb-context>)) #t)))

(test-group "default-includes"
  (let ((ctx (make <xkb-context>)))
    (test-equal #f (null? (include-paths ctx)))))

(test-group "no-default-includes"
  (let ((ctx (make <xkb-context> #:no-default-includes? #t)))
    (test-equal '() (include-paths ctx))))

(test-group "no-environment-names"
  (test-assert (make <xkb-context> #:no-environment-names? #t)))

(test-group "bad keyword"
  (test-error (make <xkb-context> #:no-environment-names #t)))

(test-group "include-path-append"
  (let ((ctx (make <xkb-context>))
        (curdir (dirname (current-filename))))
    (test-assert (include-path-append ctx curdir))
    (test-assert (member curdir (include-paths ctx)))))

(test-group "include-path-append invalid-path"
  (let ((ctx (make <xkb-context>)))
    (test-equal #f (include-path-append ctx "./this-should-not-exist"))))

(test-group "include-path-append-default"
  (let ((ctx (make <xkb-context>)))
    (test-assert (include-path-append-default ctx))
    (let* ((paths (include-paths ctx))
           (fp (car paths)))
      (test-equal 2 (length (filter (λ (p) (equal? p fp)) paths))))))

(test-group "include-path-reset-defaults"
  (let ((ctx (make <xkb-context>))
        (curdir (dirname (current-filename))))
    (test-assert (include-path-append ctx curdir))
    (test-assert (include-path-reset-defaults ctx))
    (test-equal #f (member curdir (include-paths ctx)))))

(test-group "include-path-clear"
  (let ((ctx (make <xkb-context>)))
    (test-assert (include-path-append ctx (dirname (current-filename))))
    (include-path-clear ctx)
    (test-equal '() (include-paths ctx))))

(test-group "log-level"
  (let ((ctx (make <xkb-context>)))
    (set-log-level! ctx 33)
    (test-equal 33 (log-level ctx))))

(test-group "log-level enums"
  (let ((ctx (make <xkb-context>)))
    (for-each
      (λ (level)
        (set-log-level! ctx level)
        (test-equal level (log-level ctx)))
      (list
        XKB_LOG_LEVEL_CRITICAL
        XKB_LOG_LEVEL_ERROR
        XKB_LOG_LEVEL_WARNING
        XKB_LOG_LEVEL_INFO
        XKB_LOG_LEVEL_DEBUG))))

(test-group "log-verbosity"
  (let ((ctx (make <xkb-context>)))
    (set-log-verbosity! ctx 8)
    (test-equal 8 (log-verbosity ctx))))

(test-group "set-log-fn!"
  (let ((ctx (make <xkb-context>))
        (log-count 0))
    (set-log-fn! ctx (λ (level str) (set! log-count (1+ log-count))))
    (test-error (make <xkb-keymap> ctx "bad-keymap}"))
    (test-equal 2 log-count)))

(test-group "set-log-fn! bad"
  (let ((ctx (make <xkb-context>))
        (log-count 0))
    (set-log-fn! ctx (λ (level str)
                  (throw 'misc-error '())
                  (set! log-count (1+ log-count))))
    (test-error (make <xkb-keymap> ctx "bad-keymap}"))
    (test-equal 0 log-count)))

(test-group "set-log-fn! #f"
  (let ((ctx (make <xkb-context>))
        (log-count 0))
    (set-log-fn! ctx (λ (level str) (set! log-count (1+ log-count))))
    (set-log-fn! ctx #f)
    (test-error (make <xkb-keymap> ctx "bad-keymap}"))
    (test-equal 0 log-count)))

(exit (if (zero? (test-runner-fail-count (test-end))) 0 1))
