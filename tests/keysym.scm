;;;; -*- coding: utf-8; mode: scheme -*-
;;;; SPDX-FileCopyrightText: 2020 Jason Francis <jason@cycles.network>
;;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (xkbcommon tests keysym)
  #:use-module (xkbcommon)
  #:use-module (xkbcommon keysyms)
  #:use-module (srfi srfi-64)
  #:duplicates (merge-generics))

(module-set! (resolve-module '(srfi srfi-64)) 'test-log-to-file #f)
(test-begin "xkbcommon")

(test-group "xkb-keysym-get-name"
  (test-equal "BackSpace" (xkb-keysym-get-name XKB_KEY_BackSpace)))

(test-group "xkb-keysym-from-name"
  (test-equal XKB_KEY_BackSpace (xkb-keysym-from-name "BackSpace")))

(test-group "xkb-keysym-to-upper"
  (test-equal XKB_KEY_A (xkb-keysym-to-upper XKB_KEY_a)))

(test-group "xkb-keysym-to-lower"
  (test-equal XKB_KEY_a (xkb-keysym-to-lower XKB_KEY_A)))

(test-group "xkb-keysym-to-utf8"
  (test-equal "a" (xkb-keysym-to-utf8 XKB_KEY_a)))

(test-group "xkb-keysym-to-utf32"
  (test-equal #\a (xkb-keysym-to-utf32 XKB_KEY_a)))

(test-group "xkb-utf32-to-keysym"
  (test-equal XKB_KEY_a (xkb-utf32-to-keysym #\a)))

(test-group "xkb-keysym-from-symbol"
  (test-equal XKB_KEY_a (xkb-keysym-from-symbol 'XKB_KEY_a)))

(test-group "xkb-keysym-get-symbol"
  (test-equal 'XKB_KEY_a (xkb-keysym-get-symbol XKB_KEY_a)))

(test-group "xkb-keysym-to-utf8 all"
  (hash-for-each (λ (key val) (test-assert (xkb-keysym-to-utf8 val)))
                 xkb-keysym-hash))

(test-group "xkb-keysym-to-utf32 all"
  (hash-for-each (λ (key val)
                    (let ((char (xkb-keysym-to-utf32 val)))
                      (test-assert (or char
                                       (string-null?
                                         (xkb-keysym-to-utf8 val))))))
                 xkb-keysym-hash))

(test-group "xkb-utf32-to-keysym all"
  (hash-for-each (λ (key val)
                   (let ((char (xkb-keysym-to-utf32 val)))
                     (when char
                       (test-assert (integer? (xkb-utf32-to-keysym char))))))
                 xkb-keysym-hash))

(exit (if (zero? (test-runner-fail-count (test-end))) 0 1))

