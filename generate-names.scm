;;;; -*- coding: utf-8; mode: scheme -*-
;;;; SPDX-FileCopyrightText: 2020 Jason Francis <jason@cycles.network>
;;;; SPDX-License-Identifier: GPL-3.0-or-later

(use-modules (ice-9 format) (ice-9 rdelim) (ice-9 regex))

(define (each-input-line proc)
  (let loop ((line (read-line)))
    (unless (eof-object? line)
      (proc line)
      (loop (read-line)))))

(define xkb-name-re (make-regexp "(XKB_[A-Z]+_NAME_[A-Z0-9]+)[ \t]+(.+)"))

(define (display-c-define name type)
  (format #t
" static const char *s_~32a = ~34a; scm_c_define(s_~@*~32a, scm_from_~3@*~a(~2@*~10a)); scm_c_export(s_~@*~32a, NULL);\n"
    name (format #f "\"~a\"" name) name type))

(define (type-for-name name)
  (if (string-prefix? "XKB_LED_" name) "utf8_string" "utf8_symbol"))

(define (print-file)
  (define name-list '())
  (each-input-line
    (λ (line)
       (and=> (regexp-exec xkb-name-re line)
         (λ (m) (set! name-list (cons (match:substring m 1) name-list))))))
  (set! name-list (reverse! name-list))
  (for-each (λ (name) (display-c-define name (type-for-name name))) name-list))

(apply
  (case-lambda
    ((_ names-header outfile)
     (with-output-to-file outfile
       (λ () (with-input-from-file names-header print-file))))
    ((name . _)
     (format (current-error-port) "Usage: ~a XKB-NAMES-H OUTPUT-SCM" name)
     (quit 1)))
  (command-line))
