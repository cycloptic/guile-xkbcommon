;;;; -*- coding: utf-8; mode: scheme -*-
;;;; SPDX-FileCopyrightText: 2020 Jason Francis <jason@cycles.network>
;;;; SPDX-License-Identifier: GPL-3.0-or-later

(use-modules (ice-9 format) (ice-9 rdelim) (ice-9 regex)
             (srfi srfi-1) (srfi srfi-26))

(define (each-input-line proc)
  (let loop ((line (read-line)))
    (unless (eof-object? line)
      (proc line)
      (loop (read-line)))))

(define c-sym-re (make-regexp "[^a-zA-Z0-9_]"))
(define xkb-keysym-res
  (list
    (make-regexp "^#define (XKB_KEY_[a-zA-Z_0-9]+)[ \t]+0x([0-9a-f]+)[ \t]*/\\* U+([0-9A-F]{4,6}) (.*) \\*/[ \t]*$")
    (make-regexp "^#define (XKB_KEY_[a-zA-Z_0-9]+)[ \t]+0x([0-9a-f]+)[ \t]*/\\*\\(U+([0-9A-F]{4,6}) (.*)\\)\\*/[ \t]*$")
    (make-regexp "^#define (XKB_KEY_[a-zA-Z_0-9]+)[ \t]+0x([0-9a-f]+)[ \t]*(/\\*[ \t]*(.*)[ \t]*\\*/)?[ \t]*$")))

(define (display-c-define name)
  (format #t
" static const char *s_~32a = ~34a; scm_c_define(s_~@*~32a, scm_from_~3@*~a(~2@*~10a)); scm_c_export(s_~@*~32a, NULL);\n"
    (regexp-substitute/global #f c-sym-re name 'pre "_" 'post)
    (format #f "\"~a\"" name)
    name "uint32"))

(define (print-file)
  (define sym-list '())
  (each-input-line
    (λ (line)
       (and=> (any (cut regexp-exec <> line) xkb-keysym-res)
         (λ (m) (set! sym-list (cons (match:substring m 1) sym-list))))))
  (set! sym-list (reverse! sym-list))
  (for-each (λ (sym) (display-c-define sym)) sym-list))

(apply
  (case-lambda
    ((_ keysym-header outfile)
     (with-output-to-file outfile
       (λ () (with-input-from-file keysym-header print-file))))
    ((name . _)
     (format (current-error-port)
             "Usage: ~a XKB-KEYSYM-H OUTPUT-SCM" name)
     (quit 1)))
  (command-line))

