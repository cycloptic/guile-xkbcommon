;;;; -*- coding: utf-8; mode: scheme -*-
;;;; SPDX-FileCopyrightText: 2020 Jason Francis <jason@cycles.network>
;;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (xkbcommon)
  #:use-module (ice-9 hash-table)
  #:use-module (oop goops)
  #:use-module (srfi srfi-1)
  #:export (include-path-append         include-path-append-default
            include-path-reset-defaults include-path-clear
            include-paths               log-level
            set-log-level!              log-verbosity
            set-log-verbosity!          set-log-fn!

            rules model layout variant options

            ->string     min-keycode  max-keycode
            key-for-each  key-map     keys        key-get-name key-by-name
            mods mod-flag mod-flags   mod-index
            layouts                   layout-index
            leds                      led-index
            num-layouts-for-key      num-levels-for-key
            key-get-mods-for-level   key-get-syms-by-level
            key-repeats?

            keymap         update-key     update-mask      key-get-syms
            key-get-string key-get-char   key-get-one-sym  key-get-layout
            key-get-level  serialize-mods serialize-layout
            mod-active?    key-get-consumed-mods           mod-consumed?
            layout-active? led-active?

            xkb-keysym-hash        xkb-keysym-value-hash
            xkb-keysym-from-symbol xkb-keysym-get-symbol

            <xkb-context> <xkb-rule-names> <xkb-keymap> <xkb-state>

            XKB_LOG_LEVEL_CRITICAL XKB_LOG_LEVEL_ERROR XKB_LOG_LEVEL_WARNING
            XKB_LOG_LEVEL_INFO     XKB_LOG_LEVEL_DEBUG

            XKB_STATE_MODS_DEPRESSED   XKB_STATE_MODS_LATCHED
            XKB_STATE_MODS_LOCKED      XKB_STATE_MODS_EFFECTIVE
            XKB_STATE_LAYOUT_DEPRESSED XKB_STATE_LAYOUT_LATCHED
            XKB_STATE_LAYOUT_LOCKED    XKB_STATE_LAYOUT_EFFECTIVE
            XKB_STATE_LEDS

            XKB_MOD_NAME_SHIFT XKB_MOD_NAME_CAPS XKB_MOD_NAME_CTRL
            XKB_MOD_NAME_ALT   XKB_MOD_NAME_NUM  XKB_MOD_NAME_LOGO
            XKB_LED_NAME_CAPS  XKB_LED_NAME_NUM  XKB_LED_NAME_SCROLL

            xkb-keysym-get-name xkb-keysym-from-name xkb-keysym-to-upper
            xkb-keysym-to-lower xkb-keysym-to-utf8   xkb-keysym-to-utf32
            xkb-utf32-to-keysym)
#:re-export (initialize))

(eval-when (expand load eval compile)
  (load-extension "libguile-xkbcommon" "scm_init_xkbcommon"))

;;; keysyms

(define xkb-keysym-hash
  (alist->hash-table (module-map (λ (sym var) (cons sym (variable-ref var)))
                                 (resolve-interface '(xkbcommon keysyms)))))

(define xkb-keysym-value-hash
  (alist->hash-table (module-map (λ (sym var) (cons (variable-ref var) sym))
                                 (resolve-interface '(xkbcommon keysyms)))))

(define (xkb-keysym-from-symbol symbol)
  (hash-ref xkb-keysym-hash symbol))

(define (xkb-keysym-get-symbol sym)
  (hashq-ref xkb-keysym-value-hash sym))

;;; <xkb-context>

(define-method (initialize (context <xkb-context>) args)
  (let ((other (apply make-xkb-context args)))
    (slot-set! context 'context (slot-ref other 'context))
    (slot-set! other 'context 0)))

(define-method (include-path-append (context <xkb-context>) path)
  (xkb-context-include-path-append context path))

(define-method (include-path-append-default (context <xkb-context>))
  (xkb-context-include-path-append-default context))

(define-method (include-path-reset-defaults (context <xkb-context>))
  (xkb-context-include-path-reset-defaults context))

(define-method (include-path-clear (context <xkb-context>))
  (xkb-context-include-path-clear context))

(define-method (include-paths (context <xkb-context>))
  (xkb-context-include-paths context))

(define-method (set-log-level! (context <xkb-context>) level)
  (xkb-context-set-log-level! context level))

(define-method (log-level (context <xkb-context>))
  (xkb-context-get-log-level context))

(define-method (set-log-verbosity! (context <xkb-context>) verbosity)
  (xkb-context-set-log-verbosity! context verbosity))

(define-method (log-verbosity (context <xkb-context>))
  (xkb-context-get-log-verbosity context))

(define-method (set-log-fn! (context <xkb-context>) proc)
  (xkb-context-set-log-fn! context proc))

;;; <xkb-rule-names>

(define-syntax add-accessors
  (syntax-rules ()
    ((_ class)
     ((@@ (oop goops) compute-slot-accessors) class (class-slots class)))
    ((_ class name rest ...)
     (begin
       (define-accessor name)
       (slot-set! (class-slot-definition class 'name) 'accessor name)
       (add-accessors class rest ...)))))

(add-accessors <xkb-rule-names> rules model layout variant options)

(define-method (initialize (names <xkb-rule-names>) args)
  (apply (lambda* (#:key rules model layout variant options)
           (next-method names args)) args))

;;; <xkb-keymap>

(define-method (initialize (keymap <xkb-keymap>) args)
  (let ((other (apply make-xkb-keymap args)))
    (slot-set! keymap 'keymap (slot-ref other 'keymap))
    (slot-set! other 'keymap 0)))

(define-method (->string (keymap <xkb-keymap>))
  (xkb-keymap->string keymap))

(define-method (min-keycode (keymap <xkb-keymap>))
  (xkb-keymap-min-keycode keymap))

(define-method (max-keycode (keymap <xkb-keymap>))
  (xkb-keymap-max-keycode keymap))

(define-method (key-for-each (keymap <xkb-keymap>) proc)
  (xkb-keymap-key-for-each keymap proc))

(define-method (key-map (keymap <xkb-keymap>) proc)
  (let* ((acc '())
         (f (λ (key) (set! acc (cons (proc key) acc)))))
    (xkb-keymap-key-for-each keymap f)
    (reverse acc)))

(define-method (keys (keymap <xkb-keymap>))
  (key-map keymap identity))

(define-method (key-get-name (keymap <xkb-keymap>) key)
  (xkb-keymap-key-get-name keymap key))

(define-method (key-by-name (keymap <xkb-keymap>) name)
  (xkb-keymap-key-by-name keymap name))

(define-method (mods (keymap <xkb-keymap>))
  (xkb-keymap-mods keymap))

(define-method (mod-flag (keymap <xkb-keymap>) mod)
  (let ((index (mod-index keymap mod)))
    (if index (ash 1 index) #f)))

(define-method (mod-flags (keymap <xkb-keymap>))
  (define counter 0)
  (map-in-order (λ (m)
                  (let ((c counter))
                    (set! counter (1+ counter))
                    (cons m (ash 1 c))))
                (mods keymap)))

(define-method (mod-index (keymap <xkb-keymap>) mod)
  (list-index (λ (m) (equal? m mod)) (xkb-keymap-mods keymap)))

(define-method (layouts (keymap <xkb-keymap>))
  (xkb-keymap-layouts keymap))

(define-method (layout-index (keymap <xkb-keymap>) layout)
  (list-index (λ (m) (equal? m layout)) (xkb-keymap-layouts keymap)))

(define-method (leds (keymap <xkb-keymap>))
  (xkb-keymap-leds keymap))

(define-method (led-index (keymap <xkb-keymap>) led)
  (list-index (λ (m) (equal? m led)) (xkb-keymap-leds keymap)))

(define-method (num-layouts-for-key (keymap <xkb-keymap>) key)
  (xkb-keymap-num-layouts-for-key keymap key))

(define-method (num-levels-for-key (keymap <xkb-keymap>) key)
  (xkb-keymap-num-layouts-for-key keymap key))

(define-method (key-get-mods-for-level (keymap <xkb-keymap>) key layout level)
  (xkb-keymap-key-get-mods-for-level keymap key layout level))

(define-method (key-get-syms-by-level (keymap <xkb-keymap>) key layout level)
  (xkb-keymap-key-get-syms-by-level keymap key layout level))

(define-method (key-repeats? (keymap <xkb-keymap>) key)
  (xkb-keymap-key-repeats? keymap key))

;;; <xkb-state>

(define-method (initialize (state <xkb-state>) args)
  (let ((other (apply make-xkb-state args)))
    (slot-set! state 'state (slot-ref other 'state))
    (slot-set! other 'state 0)))

(define-method (update-key (state <xkb-state>) key direction)
  (xkb-state-update-key state key direction))

(define-method (update-mask (state <xkb-state>)
                            depressed-mods latched-mods locked-mods
                            depressed-layout latched-layout locked-layout)
  (xkb-state-update-mask state depressed-mods latched-mods locked-mods
                               depressed-layout latched-layout locked-layout))

(define-method (key-get-syms (state <xkb-state>) key)
  (xkb-state-key-get-syms state key))

(define-method (key-get-string (state <xkb-state>) key)
  (xkb-state-key-get-string state key))

(define-method (key-get-char (state <xkb-state>) key)
  (xkb-state-key-get-char state key))

(define-method (key-get-one-sym (state <xkb-state>) key)
  (xkb-state-key-get-one-sym state key))

(define-method (key-get-layout (state <xkb-state>) key)
  (xkb-state-key-get-layout state key))

(define-method (key-get-level (state <xkb-state>) key layout)
  (xkb-state-key-get-level state key layout))

(define-method (serialize-mods (state <xkb-state>) components)
  (xkb-state-serialize-mods state components))

(define-method (serialize-layout (state <xkb-state>) components)
  (xkb-state-serialize-layout state components))

(define-method (mod-active? (state <xkb-state>) mod type)
  (xkb-state-mod-active? state mod type))

(define-method (key-get-consumed-mods (state <xkb-state>) key . flags)
  (apply xkb-state-key-get-consumed-mods state key flags))

(define-method (mod-consumed? (state <xkb-state>) key . flags)
  (apply xkb-state-mod-consumed? state key flags))

(define-method (layout-active? (state <xkb-state>) layout type)
  (xkb-state-layout-active? state layout type))

(define-method (led-active? (state <xkb-state>) led)
  (xkb-state-led-active? state led))

