/* SPDX-FileCopyrightText: 2020 Jason Francis <jason@cycles.network>
 * SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef _GUILE_XKBCOMMON_H
#define _GUILE_XKBCOMMON_H

#include <libguile/scm.h>

SCM_API SCM scm_xkb_rule_names_type;
SCM_API SCM scm_xkb_context_type;
SCM_API SCM scm_xkb_keymap_type;
SCM_API SCM scm_xkb_state_type;

#define SCM_VALIDATE_XKB_CONTEXT_COPY(pos, k, cvar) \
  do { \
    SCM_ASSERT_TYPE(SCM_IS_A_P(k, scm_xkb_context_type), \
        k, pos, FUNC_NAME, "<xkb-context>"); \
    cvar = scm_foreign_object_ref(k, 0); \
    SCM_ASSERT_TYPE(cvar, k, pos, FUNC_NAME, "non-null <xkb-context>"); \
  } while (0)

#define SCM_VALIDATE_XKB_KEYMAP_COPY(pos, k, cvar) \
  do { \
    SCM_ASSERT_TYPE(SCM_IS_A_P(k, scm_xkb_keymap_type), \
        k, pos, FUNC_NAME, "<xkb-keymap>"); \
    cvar = scm_foreign_object_ref(k, 0); \
    SCM_ASSERT_TYPE(cvar, k, pos, FUNC_NAME, "non-null <xkb-keymap>"); \
  } while (0)

#define SCM_VALIDATE_XKB_STATE_COPY(pos, k, cvar) \
  do { \
    SCM_ASSERT_TYPE(SCM_IS_A_P(k, scm_xkb_state_type), \
        k, pos, FUNC_NAME, "<xkb-state>"); \
    cvar = scm_foreign_object_ref(k, 0); \
    SCM_ASSERT_TYPE(cvar, k, pos, FUNC_NAME, "non-null <xkb-state>"); \
  } while (0)

SCM_API SCM scm_xkb_keysym_get_name(SCM keysym);
SCM_API SCM scm_xkb_keysym_from_name(SCM name, SCM rest);
SCM_API SCM scm_xkb_keysym_to_upper(SCM keysym);
SCM_API SCM scm_xkb_keysym_to_lower(SCM keysym);
SCM_API SCM scm_xkb_keysym_to_utf8(SCM keysym);
SCM_API SCM scm_xkb_keysym_to_utf32(SCM keysym);
SCM_API SCM scm_xkb_utf32_to_keysym(SCM ucs);
SCM_API SCM scm_make_xkb_context(SCM rest);
SCM_API SCM scm_xkb_context_unref(SCM context);
SCM_API SCM scm_xkb_context_include_path_append(SCM context, SCM path);
SCM_API SCM scm_xkb_context_include_path_append_default(SCM context);
SCM_API SCM scm_xkb_context_include_path_reset_defaults(SCM context);
SCM_API SCM scm_xkb_context_include_path_clear(SCM context);
SCM_API SCM scm_xkb_context_include_paths(SCM context);
SCM_API SCM scm_xkb_context_set_log_level_x(SCM context, SCM level);
SCM_API SCM scm_xkb_context_get_log_level(SCM context);
SCM_API SCM scm_xkb_context_set_log_verbosity_x(SCM context, SCM verbosity);
SCM_API SCM scm_xkb_context_get_log_verbosity(SCM context);
SCM_API SCM scm_xkb_context_set_log_fn_x(SCM context, SCM log_fn);

SCM_API SCM scm_make_xkb_rule_names(SCM rest);
SCM scm_c_make_xkb_rule_names(const char *rules, const char *model,
    const char *layout, const char *variant, const char *options);

SCM_API SCM scm_make_xkb_keymap(SCM context, SCM rest);
SCM_API SCM scm_xkb_keymap_unref(SCM keymap);
SCM_API SCM scm_xkb_keymap_to_string(SCM keymap);
SCM_API SCM scm_xkb_keymap_min_keycode(SCM keymap);
SCM_API SCM scm_xkb_keymap_max_keycode(SCM keymap);
SCM_API SCM scm_xkb_keymap_key_for_each(SCM keymap, SCM proc);
SCM_API SCM scm_xkb_keymap_key_get_name(SCM keymap, SCM key);
SCM_API SCM scm_xkb_keymap_key_by_name(SCM keymap, SCM name);
SCM_API SCM scm_xkb_keymap_mods(SCM keymap);
SCM_API SCM scm_xkb_keymap_layouts(SCM keymap);
SCM_API SCM scm_xkb_keymap_leds(SCM keymap);
SCM_API SCM scm_xkb_keymap_num_layouts_for_key(SCM keymap, SCM key);
SCM_API SCM scm_xkb_keymap_num_levels_for_key(SCM keymap, SCM key, SCM layout);
SCM_API SCM scm_xkb_keymap_key_get_mods_for_level(SCM keymap, SCM key,
    SCM layout, SCM level);
SCM_API SCM scm_xkb_keymap_key_get_syms_by_level(SCM keymap, SCM key,
    SCM layout, SCM level);
SCM_API SCM scm_xkb_keymap_key_repeats(SCM keymap, SCM key);

SCM_API SCM scm_make_xkb_state(SCM keymap);
SCM_API SCM scm_xkb_state_unref(SCM state);
SCM_API SCM scm_xkb_state_update_key(SCM state, SCM key, SCM pressed);
SCM_API SCM scm_xkb_state_update_mask(SCM state,
    SCM depressed_mods, SCM latched_mods, SCM locked_mods,
    SCM depressed_layout, SCM latched_layout, SCM locked_layout);
SCM_API SCM scm_xkb_state_key_get_syms(SCM state, SCM key);
SCM_API SCM scm_xkb_state_key_get_string(SCM state, SCM key);
SCM_API SCM scm_xkb_state_key_get_char(SCM state, SCM key);
SCM_API SCM scm_xkb_state_key_get_one_sym(SCM state, SCM key);
SCM_API SCM scm_xkb_state_key_get_layout(SCM state, SCM key);
SCM_API SCM scm_xkb_state_key_get_level(SCM state, SCM key, SCM layout);
SCM_API SCM scm_xkb_state_serialize_mods(SCM state, SCM components);
SCM_API SCM scm_xkb_state_serialize_layout(SCM state, SCM components);
SCM_API SCM scm_xkb_state_mod_active_p(SCM state, SCM mod, SCM type);
SCM_API SCM scm_xkb_state_key_get_consumed_mods(SCM state, SCM key, SCM rest);
SCM_API SCM scm_xkb_state_mod_consumed_p(SCM state, SCM key, SCM index,
    SCM rest);
SCM_API SCM scm_xkb_state_layout_active_p(SCM state, SCM layout, SCM type);
SCM_API SCM scm_xkb_state_led_active_p(SCM state, SCM led);

SCM_API void scm_init_xkbcommon(void);
SCM_API void scm_init_xkbcommon_keysyms(void);

#endif
